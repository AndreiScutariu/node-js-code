const joi = require("joi")

const schema = joi.object({
     PORT_NO: joi.number().integer().min(0).max(65535).required()
}).unknown().required()

const envVars = joi.attempt(process.env, schema)

module.exports = {
     port: envVars.PORT_NO || 3000
}