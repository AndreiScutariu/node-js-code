const { Router } = require("express")
const users = require("../users")

const router = new Router()

router.get("/users", users.get)
router.get("/users/:id", users.getById)

module.exports = router