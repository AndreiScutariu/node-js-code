require("dotenv").config()

const express = require("express")
const promisify = require("es6-promisify")
const http = require("http")
const config = require("./config")
const router = require("./routes")
const { errorHandler } = require('./utils')

const app = express()
const server = http.createServer(app)

// setting a middlesware
// app.use((req, res, next) => {
//      console.log("before request")
//      next()
//      console.log("after request")
// })

app.use(router)
app.use(errorHandler)

process.on("SIGTERM", async () => {
     console.log("closing the app ...");
     await close();
     process.exit(0);
})

const initApp = promisify(server.listen, server)  

async function init() {
     await initApp(config.port)
     console.log("app is running on " + config.port + " port")
}

async function close() {
     try {
          server.close()
     } catch (err) {
          console.log(err)
     }
}

init()