"use strict";

const nock = require("nock");
const { expect } = require("chai");
const server = require("../../server");
const request = require("super-request");
const sinon = require("sinon");

const userModel = require("../../../models/user");

describe("GET api/v1/users",
    () => {

        let sandbox;

        beforeEach(() => {
            sandbox = sinon.sandbox.create();
        });

        afterEach(() => {
            sandbox.restore();
        });

        it("should get the users from the user model",
            async () => {
                const getUsers = sandbox.stub(userModel, "getUsers").returns({ users: [] });
                const users = await request(server)
                    .get("/api/v1/users?q=bo")
                    .expect(200, { users: [] })
                    .end();
                expect(getUsers).to.be.calledOnce;
            });
    });