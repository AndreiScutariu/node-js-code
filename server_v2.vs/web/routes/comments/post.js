"use strict";

const commentModel = require("../../../models/comment");
const apicache = require("apicache");

async function addComment(req, res) {
    console.log("req body", req.body);
    //apicache.clear(req.body.user);
    await commentModel.addComment(req.body);
    res.status(201).send("Comment Added Successfully");
}

module.exports = addComment;