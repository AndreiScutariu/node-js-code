"use strict";

function catchAsyncErrors(middleware) {
    return (req, res, next) => Promise.resolve(middleware(req, res, next)).catch(next);
}

function errorHandler(err, req, res, next) {
    console.error("error", err);
    if (err.response !== undefined && err.response.status === 404) {
        res.status(404).send("Oops! Not found!");
    } else {
        res.status(500).send("Oops! Internal Server Error.");
    }
}

module.exports = {
    catchAsyncErrors,
    errorHandler
};