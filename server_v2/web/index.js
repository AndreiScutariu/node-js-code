'use strict'

const promisify = require('es6-promisify')
const server = require('./server')
const config = require('./config')
const http = require('http')
const db = require('../models/db')

process.on('SIGTERM', async () => {
    const exitCode = await stop()
    process.exit(exitCode)
})

const initServer = promisify(server.listen, server)
async function init() {
    try {
        await db.init()
        await initServer(config.port)
    } catch (err) {
        console.log(`Couldn't init the app: ${err}`)
        process.exit(1)
    }
    console.log(`App is listening on port ${config.port}`)
}

const closeServer = promisify(server.close, server)
async function stop() {
    let exitCode = 0
    try {
        await closeServer()
    } catch (err) {
        console.log(`Failed to close the server: ${err}`)
        exitCode = 1
    }

    try {
        await db.close()
    } catch (err) {
        console.log(`Failed to close the server: ${err}`)
        exitCode = 1
    }

    return exitCode
}

module.exports = {
    init,
    stop
}
