'use strict'

// use different env vars for the tests!
require('dotenv').config({ path: '.env.test' })

const chai = require('chai')
const sinonChai = require('sinon-chai')
const chaiSubset = require('chai-subset')

chai.use(sinonChai)
chai.use(chaiSubset)

const app = require('../web')

before(async () => {
     await app.init()
})

after(async () => {
     await app.stop()
})